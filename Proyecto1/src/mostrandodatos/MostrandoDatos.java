package mostrandodatos;

import java.util.Scanner;

public class MostrandoDatos {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		
		String nombre;
		String apellidos;
		System.out.println("Cual es tu nombre");
		nombre=in.nextLine();
		System.out.println("Cuales son tus apellidos");
		apellidos=in.nextLine();
		
		System.out.println("Tu nombre es " + nombre + " y tus apellidos son " + apellidos);

	}

}
